#--------------------------------------------
#   BUILD BACKEND
#--------------------------------------------
FROM --platform=${TARGETPLATFORM} golang:1.16-alpine AS api-builder
ARG TARGETPLATFORM
ARG TARGETARCH
ARG TARGETVARIANT

COPY backend /go/src/app
WORKDIR /go/src/app
RUN GOARCH=${TARGETARCH} go build -o /app/build/server . 

#--------------------------------------------
#   BUILD FRONTEND
#--------------------------------------------
FROM --platform=${TARGETPLATFORM} node:lts-alpine3.14 AS app-builder
ARG TARGETPLATFORM
ARG TARGETARCH
ARG TARGETVARIANT

WORKDIR /app
COPY frontend/package.json .
RUN yarn install

COPY frontend . 
RUN yarn build

# ---------------------------------
#   RELEASE
# ---------------------------------
FROM --platform=${TARGETPLATFORM} alpine
RUN mkdir /app

ARG TARGETPLATFORM
ARG TARGETARCH
ARG TARGETVARIANT

# RUN \
#   TINI_VERSION=v0.14.0 && \
#   TINI_ARCH=${TARGETARCH} && \
#   gpg --keyserver ha.pool.sks-keyservers.net --recv-keys 0x595E85A6B1B4779EA4DAAEC70B588DFF0527A9B7 && \
#   curl -fsSLo /sbin/tini "https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini${TINI_ARCH:+-$TINI_ARCH}" && \
#   curl -fsSL "https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini${TINI_ARCH:+-$TINI_ARCH}.asc" | \
#     gpg --verify - /sbin/tini && \
#   chmod +x /sbin/tini

COPY --from=api-builder /app/build/server /app/server
COPY --from=app-builder /app/build /app/web

# PERMISSIONS - create user (appuser) and group (1000)
RUN addgroup -S 1000 && \
    adduser -S -D -H appuser -G 1000

# RUN adduser appuser -D -H
RUN chown -R appuser /app \
    && chmod g+r /app
    #  \
    # && chmod g+w /app/config 

USER appuser

# https://github.com/krallin/tini
# ENTRYPOINT [ "/sbin/tini", "--" ]

# Run the server executable
CMD [ "/app/server", "serve" ]