TAG=latest
REPO=registry.raspi.k8s/
REPO_PATH=#{REPO_PATH}/#{NAME}

buildx:
	docker buildx build --push --platform linux/arm64/v8,linux/amd64 -t $(REPO)$(REPO_PATH):$(TAG) . --builder locbuilder --no-cache --rm
