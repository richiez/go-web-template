package handlers

import (
	"net/http"
	"#{NAME}/api/models"

	"github.com/labstack/echo/v4"
)

// GetConfig godoc
// @Summary Gets config deails
// @Description Gets current status of config
// @Tags config
// @Accept json
// @Produce json
//// @Security ApiKeyAuth
// @Success 200 {object} APIResponse{data=boolean}
// @Failure 503 {object} APIResponse{}
// @Router /v1/config [get]
func Ping(c echo.Context) error {
	return c.JSONPretty(http.StatusOK, models.APIResponse{
		Success: true,
		Message: "ping",
		Data:    true,
	}, "  ")
}
